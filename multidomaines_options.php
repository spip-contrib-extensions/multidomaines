<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/config');
$options  = lire_config('multidomaines');
$GLOBALS['multidomaine_id_secteur_courant'] = null;
$GLOBALS["multidomaine_site_principal"] = true;
if (!defined('_MULTIDOMAINE_RACINE')) {
	define('_MULTIDOMAINE_RACINE', '0');
}
if (!defined('_MULTIDOMAINE_RUBRIQUE')) {
	define('_MULTIDOMAINE_RUBRIQUE', '0');
}
if (!defined('_SECTEUR_URL')) {
	define('_SECTEUR_URL', '0');
}

if (is_array($options)) {
	foreach ($options as $id_rubrique => $config) {
		if ($id_rubrique != 'defaut') {
			if (!isset($config['url']) or !$config['url']) {
				$url = $options['defaut']['url'];
			}
			else {
				$url = $config['url'];
			}
			
			$partie_url = parse_url($url);
			if (!isset($partie_url['port'])) {
				$partie_url['port'] = $partie_url['scheme'] == 'https' ? 443 : 80;
			}
			$host = (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '');
			if (defined('_MULTIDOMAINE_IGNORE_WWW') && _MULTIDOMAINE_IGNORE_WWW) {
				$host = preg_replace('/^www\./','',$host);
				$partie_url['host'] = preg_replace('/^www\./','',$partie_url['host']);
			}
			if ($partie_url['host'] == $host AND $partie_url['port'] == $_SERVER['SERVER_PORT']) {
				if (isset($config['squelette']) and $config['squelette']) {
					$GLOBALS['multidomaine_id_secteur_courant'] = $id_rubrique;
					$GLOBALS['dossier_squelettes'] = trim($GLOBALS['dossier_squelettes'] . ':' . $config['squelette'], ':');
					$GLOBALS["multidomaine_site_principal"] = false;
				}
			}
		}
	}
	if (!$GLOBALS['dossier_squelettes']) {
		multidomaines_squelettespardefaut_dist();
	}
}

function multidomaines_squelettespardefaut_dist() {
	if (function_exists('multidomaines_squelettespardefaut')) {
		return multidomaines_squelettespardefaut();
	}
	$dossiers_port = '';
	$dossiers = '';
	$host = (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '');
	if (defined('_MULTIDOMAINE_IGNORE_WWW') && _MULTIDOMAINE_IGNORE_WWW) {
		$host = preg_replace('/^www\./','',$host);
	}
	
	if (strpos($host, '.') === false) {
		// ex: localhost
		$dossiers = ':' . lire_config('multidomaines/defaut/squelette') . '/' . $host;
	} else {
		$parties_domaine = explode('.', $host);
		$extention = array_pop($parties_domaine);
		do {
			$base = ':' . lire_config('multidomaines/defaut/squelette') . '/' . implode('.', $parties_domaine);
			$dossiers_port .= $base . '.' . $extention . '.' . $_SERVER['SERVER_PORT'];
			$dossiers_port .= $base . '.' . $_SERVER['SERVER_PORT'];
			$dossiers .= $base . '.' . $extention;
			$dossiers .= $base;
			array_shift($parties_domaine);
		} while (count($parties_domaine) > 0);
	}

	$GLOBALS['dossier_squelettes'] = trim($GLOBALS['dossier_squelettes'] . $dossiers_port . $dossiers, ':');
}

/**
 * Retourne l'URL du site en fonction du domaine.
 *
 * Les URLs des domaines sont configurées dans la méta
 * `multidomaines/[id_rubrique|defaut]/[url]`.
 *
 * Accepte une variable pour retourner l'url du site associée à l'identifiant
 *
 * En absence de correspondance, on utilise l'adresse par défaut
 *
 *
 * @return string
 */
function multidomaines_url_site($domaine_identifiant = null)
{
    $cfg_multidomaines = lire_config('multidomaines');
	$domaine = (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '');
	if (defined('_MULTIDOMAINE_IGNORE_WWW') && _MULTIDOMAINE_IGNORE_WWW) {
		$domaine = preg_replace('/^www\./','',$domaine);
	}
	$adresse_site = sinon($GLOBALS['meta']['adresse_site'], '.');

	//Gérer le cas du domaine par défaut
	if ($domaine_identifiant == "defaut") {
		return $adresse_site;
	}

	if (!empty($domaine_identifiant)) {
		//Obtenir explicitement un domaine sur la base de son identifiant
		foreach ($cfg_multidomaines as $multidomaine) {
			if ( in_array( 'identifiant',$multidomaine ) && $multidomaine['identifiant'] == $domaine_identifiant) {
				$adresse_site = $multidomaine['url'];
				break;
			}
		}
	} else {
		//Obtenir le domaine selon le contexte de navigation
		foreach ($cfg_multidomaines as $multidomaine) {
			$url = parse_url($multidomaine['url']);

			if (isset($url['host']) && $url['host'] == $domaine) {
				$adresse_site = $multidomaine['url'];
				break;
			}
		}
	}

    return $adresse_site;
}