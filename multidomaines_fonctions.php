<?php

if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

function multidomaine_trouver_secteur($contexte) {
	static $id_secteur_courant;
	if (!$id_secteur_courant) {
		if (isset($contexte['id_article'])) {
			$id_secteur_courant = sql_getfetsel('id_secteur', 'spip_articles', 'id_article=' . (int)$contexte['id_article']);
		} else if (isset($contexte['id_rubrique'])) {
			$id_secteur_courant = sql_getfetsel('id_secteur', 'spip_rubriques', 'id_rubrique=' . (int)$contexte['id_rubrique']);
		} else {
			//Par exemple la page sommaire d'un secteur
			include_spip('inc/config');
			$cfg = lire_config('multidomaines');
			foreach($cfg as $id_rubrique => $domaine) {
				$host = (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '');
				$domaine_host = parse_url($domaine['url'], PHP_URL_HOST);
				if (defined('_MULTIDOMAINE_IGNORE_WWW') && _MULTIDOMAINE_IGNORE_WWW) {
					$host = preg_replace('/^www\./','',$host);
					$domaine_host = preg_replace('/^www\./','',$domaine_host);
				}
				if (is_int($id_rubrique) && $domaine_host === $host) {
					$id_secteur_courant = $id_rubrique;
					break;
				}
			}
		}
	}
	return $id_secteur_courant;
}

function calculer_URL_SECTEUR($id_rubrique) {
	// mettre en cache les calculs
	static $urls_cache = array();
	if (isset($urls_cache[$id_rubrique])) {
		return $urls_cache[$id_rubrique];
	}

	// Remonter les rubriques jusqu'à trouver une URL multidomaine
	$url = null;
	include_spip('inc/config');
	$cfg = lire_config('multidomaines');
	$id_rubrique_courante = $id_rubrique;
	// Soit on est déjà dans un secteur
	if (isset($cfg[$id_rubrique]['url'])) {
		$url = $cfg[$id_rubrique]['url'];
	// Soit on remonte jusqu'au secteur
	} else {
		while (!$url && $id_rubrique_courante) {
			$id_parent = sql_getfetsel('id_parent', 'spip_rubriques', 'id_rubrique=' . intval($id_rubrique_courante));
			$url = isset($cfg[$id_parent]['url']) ? $cfg[$id_parent]['url'] : false;
			$id_rubrique_courante = $id_parent;
		}
	}

	// sinon, URL par défaut
	if (empty($url)) {
		$url = lire_config('multidomaines/defaut/url');
	}
	// Sinon, URL du site
	if (empty($url)) {
		$url = lire_config('adresse_site');
	}

	// mettre à jour le cache
	$urls_cache[$id_rubrique] = trim($url, '/') . '/';

	return $urls_cache[$id_rubrique];
}

function balise_NOM_SITE_MULTIDOMAINE_dist($p) {
	$id_rubrique = interprete_argument_balise(1, $p);
	if (strlen(trim($id_rubrique)) == 0) {
		$id_rubrique = calculer_balise('id_rubrique', $p)->code;
	}
	$p->code              = "calculer_nom_site_multidomaine(intval($id_rubrique))";
	$p->interdire_scripts = false;

	return $p;
}

function calculer_nom_site_multidomaine($id_rubrique){
	$nom_site = $GLOBALS['meta']['nom_site'];
	$cfg = lire_config('multidomaines');
	foreach ($cfg as $id_rubrique_domaine => $config) {
		if(is_int($id_rubrique_domaine) && $config['url']){
			$branche = explode(',',calcul_branche_in($id_rubrique_domaine));
			if(is_array($branche) && in_array($id_rubrique, $branche) ){
				$nom_site = sql_getfetsel('titre','spip_rubriques','id_rubrique = '.$id_rubrique_domaine);
			}
		}
	}
	$nom_site = supprimer_numero(typo($nom_site));

	return $nom_site;
}

/**
 * Compile la balise `#URL_SITE_SPIP` qui retourne l'URL du site
 * telle que définie dans la configuration.
 *
 * Peut utiliser un identifiant de domaine en premier paramètre
 * `#URL_SITE_SPIP{identifiant_domaine}`
 *
 * @balise
 *
 * @see balise_URL_SITE_SPIP_dist()
 * @note
 *     Surcharge afin de gérer des contexte de domaines
 *     En fonction de l'url de base, on ne revoie pas la même URL.
 *
 * @param Champ $p
 *                 Pile au niveau de la balise
 *
 * @return Champ
 *               Pile complétée par le code à générer
 */
function balise_URL_SITE_SPIP($p)
{
    $domaine_identifiant = interprete_argument_balise(1, $p);

    $p->code = "multidomaines_url_site($domaine_identifiant)";
    $p->code = 'spip_htmlspecialchars('.$p->code.')';
    $p->interdire_scripts = false;

    return $p;
}
