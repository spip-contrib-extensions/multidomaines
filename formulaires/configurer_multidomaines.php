<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Vérification des valeurs postées
 *
 * Normaliser les données avant le traiter : enlever les secteurs qui n'ont pas été configurés
 * afin de ne pas avoir des clés vides dans la config
 *
 * @return array
 */
function formulaires_configurer_multidomaines_verifier_dist() {

	$erreurs = [];

	if ($secteurs = sql_allfetsel(
		'id_rubrique',
		'spip_rubriques',
		'profondeur = 0' . (_MULTIDOMAINE_RUBRIQUE ? ' OR profondeur = 1' : '')
	)) {
		// Nettoyage des valeurs vides
		foreach ($secteurs as $secteur) {
			$id_rubrique = $secteur['id_rubrique'];
			$valeur = _request($id_rubrique);
			if (
				!$valeur
				|| (is_array($valeur) && !array_filter($valeur))
			) {
				set_request($id_rubrique, null);
			}
		}
	}

	return $erreurs;
}

function formulaires_configurer_multidomaines_traiter_dist() {
	// Enregistrement de la configuration
	include_spip('inc/cvt_configurer');
	$trace = cvtconf_formulaires_configurer_enregistre('configurer_multidomaines', []);
	$retours['message_ok'] = _T('config_info_enregistree') . $trace;

	// Nettoyage de la config : on retire les rubriques qui ne sont plus secteurs multidomaine
	include_spip('inc/config');
	$secteurs = sql_allfetsel(
		'id_rubrique',
		'spip_rubriques',
		'profondeur = 0' . (_MULTIDOMAINE_RUBRIQUE ? ' OR profondeur = 1' : '')
	);
	$ids_secteurs = array_column($secteurs, 'id_rubrique');
	$config = lire_config('multidomaines');
	if ($config) {
		foreach ($config as $secteur => $valeurs) {
			if (!in_array($secteur, $ids_secteurs) and $secteur != 'defaut') {
				unset($config[$secteur]);
			};
		}
		ecrire_config('multidomaines', $config);
	}

	return $retours;
}